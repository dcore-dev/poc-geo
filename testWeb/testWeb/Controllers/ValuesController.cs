﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;


namespace testWeb.Controllers
{
    [Route("geocodingRestApiNetCore/")] // localhost:port/geocodingRestApiNetCore/addressToFind
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public List<string> GetLocation(string address)
        {
            string apiKey = "AIzaSyDx-CK4ZJFmJta_l2eFwS03cHTDofCCdL4";
            Geocode prueba = new Geocode(address, apiKey);  // address and api key from geocoding api
            return prueba.Request(); // return info from google geocoding api
        }

        // get from localhost:port/geocodingRestApiNetCore/address
        [HttpGet("{address}")]
        public List<string> Get(string address)
        {
            return GetLocation(address);
        }
    }
}
