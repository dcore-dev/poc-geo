﻿using System.Text.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;


namespace testWeb
{
    public class Geocode
    {
        protected string address;
        protected string apiKey;
        protected string apiUrl;

        public Geocode(string address, string apiKey)
        {
            this.address = address;
            this.apiKey = apiKey;
            apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + this.address + "&key=" + this.apiKey;
        }

        // Method that request info from an address from geocoding api
        // request format: apiURL + json/xml (output format) + address + api key
        public List<string> Request()
        {
            var coords = new List<string>();
            WebRequest request = WebRequest.Create(apiUrl);
            try{
                WebResponse response = request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    var dataFromResponse = JsonSerializer.Deserialize<Root>(responseFromServer);
                    foreach (var data in dataFromResponse.results){
                        coords.Add("adress: " + (data.formatted_address));
                        coords.Add("lat: " + (data.geometry.location.lat).ToString());
                        coords.Add("lng: " + (data.geometry.location.lng).ToString());
                    }
                    coords.Add("status: " + dataFromResponse.status);
                    if (dataFromResponse.status == "ZERO_RESULTS")
                        coords.Add("message:" + " No results for " + address + " try to be more specific.");
                    return coords;
                }
            } catch(WebException)   // WebException is error 400 (INVALID_REQUEST response from geocoding api)
            {
                coords.Add("Error 400: Bad Request");
                return coords;
            }   
        }
    }
}
